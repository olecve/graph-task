(ns audience-republic-task.calculations-test
  (:require [clojure.test :refer :all]
            [audience-republic-task.calculations :refer :all]
            [audience-republic-task.test-graphs :as test-graphs]))

(deftest find-vertex-with-lowest-weight-test
  (is (= [:b 2]
         (find-vertex-with-lowest-weight [[:a 6]
                                          [:b 2]
                                          [:end ##Inf]]
                                         #{})))

  (is (= [:a 6]
         (find-vertex-with-lowest-weight [[:a 6]
                                          [:b 2]
                                          [:end ##Inf]]
                                         #{:b})))

  (is (= [:end ##Inf]
         (find-vertex-with-lowest-weight [[:a 6]
                                          [:b 2]
                                          [:end ##Inf]]
                                         #{:b :a})))

  (is (= nil
         (find-vertex-with-lowest-weight [[:a 6]
                                          [:b 2]
                                          [:end ##Inf]]
                                         #{:b :a :end}))))

(deftest update-costs-&-parents-test
  (is (= {:weights [[:a 5]
                    [:b 2]
                    [:end 7]]
          :parents {:a   :b
                    :b   :begin
                    :end :b}}
         (update-weights-&-parents {:weight      2
                                    :weights     [[:a 6] [:b 2] [:end ##Inf]]
                                    :parents     {:a   :begin
                                                  :b   :begin
                                                  :end nil}
                                    :neighbors   [[:end 5] [:a 3]]
                                    :vertex-name :b}))))

(deftest parents->path-test
  (is (= [:begin :b :a :end]
         (parents->path {:end :a
                         :a   :b
                         :b   :begin}
                        :begin
                        :end))))

(deftest find-weights-&-parents-test
  (testing "from 'begin' to other vertices"
    (is (= {:parents {:a     :b
                      :b     :begin
                      :begin :end
                      :end   :a}
            :weights [[:begin 16]
                      [:a 5]
                      [:b 2]
                      [:end 6]]}
           (find-weights-&-parents test-graphs/simple :begin :a)))
    (is (= {:parents {:a     :b
                      :b     :begin
                      :begin :end
                      :end   :a}
            :weights [[:begin 16]
                      [:a 5]
                      [:b 2]
                      [:end 6]]}
           (find-weights-&-parents test-graphs/simple :begin :b)))
    (is (= {:parents {:a     :b
                      :b     :begin
                      :begin :end
                      :end   :a}
            :weights [[:begin 16] [:a 5] [:b 2] [:end 6]]}
           (find-weights-&-parents test-graphs/simple :begin :end))))

  (testing "from 'a' to other vertices"
    (is (= {:parents {:a     :b
                      :b     :begin
                      :begin :end
                      :end   :a}
            :weights [[:end 1]
                      [:b 13]
                      [:begin 11]
                      [:a 16]]}
           (find-weights-&-parents test-graphs/simple :a :b)))))

(deftest find-shortest-path-test
  (testing "from 'begin' to other vertices"
    (is (= [:begin :b :a]
           (find-shortest-path test-graphs/simple :begin :a)))
    (is (= [:begin :b]
           (find-shortest-path test-graphs/simple :begin :b)))
    (is (= [:begin :b :a :end]
           (find-shortest-path test-graphs/simple :begin :end))))

  (testing "from 'a' to other vertices"
    (is (= [:a :end]
           (find-shortest-path test-graphs/simple :a :end)))
    (is (= [:a :end :begin]
           (find-shortest-path test-graphs/simple :a :begin)))
    (is (= [:a :end :begin :b]
           (find-shortest-path test-graphs/simple :a :b))))

  (testing "from 'a' to other vertices"
    (is (= [:b :a]
           (find-shortest-path test-graphs/simple :b :a)))
    (is (= [:b :a :end]
           (find-shortest-path test-graphs/simple :b :end)))
    (is (= [:b :a :end :begin]
           (find-shortest-path test-graphs/simple :b :begin))))

  (testing "from 'end' to other vertices"
    (is (= [:end :begin]
           (find-shortest-path test-graphs/simple :end :begin)))
    (is (= [:end :begin :b :a]
           (find-shortest-path test-graphs/simple :end :a)))
    (is (= [:end :begin :b]
           (find-shortest-path test-graphs/simple :end :b)))))

(deftest eccentricity-test
  (is (= 6 (eccentricity test-graphs/simple :begin)))
  (is (= 13 (eccentricity test-graphs/simple :a)))
  (is (= 14 (eccentricity test-graphs/simple :b)))
  (is (= 15 (eccentricity test-graphs/simple :end))))

(deftest radius-test
  (is (= 6 (radius test-graphs/simple))))

(deftest diameter-test
  (is (= 15 (diameter test-graphs/simple))))
