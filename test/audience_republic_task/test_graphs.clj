(ns audience-republic-task.test-graphs
  (:require [clojure.test :refer :all]))

(def original {:1 [[:2 1] [:3 2]]
               :2 [[:4 4]]
               :3 [[:4 2]]
               :4 []})

(def simple {:begin [[:a 6] [:b 2]]
             :a     [[:end 1]]
             :b     [[:end 5] [:a 3]]
             :end   [[:begin 10]]})
