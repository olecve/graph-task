(ns audience-republic-task.seq-graph-test
  (:require [clojure.test :refer :all]
            [audience-republic-task.seq-graph :refer :all]
            [audience-republic-task.test-graphs :as test-graphs]))

(deftest seq-graph-dfs-test
  (testing "traverse graph"
    (is (= [[:1 0] [:3 2] [:4 2] [:2 1]]
           (seq-graph-dfs test-graphs/original :1)))

    (is (= [[:begin 0] [:b 2] [:end 5] [:a 6]]
           (seq-graph-dfs test-graphs/simple :begin)))))

(deftest seq-graph-bfs-test
  (testing "traverse graph"
    (is (= [[:1 0] [:2 1] [:3 2] [:4 4]]
           (seq-graph-bfs test-graphs/original :1)))

    (is (= [[:begin 0] [:a 6] [:b 2] [:end 1]]
           (seq-graph-bfs test-graphs/simple :begin)))))
