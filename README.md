# How to run

## REPL

    $ lein repl

It should start REPL in audience-republic-task.core namespace with all required functions imported.
The main functions are:

    audience-republic-task.core=> (generate-graph 5 10)
    {:1 [[:2 5] [:3 3]], :2 [[:3 1] [:4 6]], :3 [[:4 6] [:1 6]], :4 [[:5 8] [:5 7]], :5 [[:1 9] [:3 4]]}

    audience-republic-task.core=> (def graph (generate-graph 5 10))
    #'audience-republic-task.core/graph
    audience-republic-task.core=> graph
    {:1 [[:2 2] [:5 2] [:3 3]], :2 [[:3 6] [:1 8]], :3 [[:4 9] [:5 3]], :4 [[:5 9] [:2 5]], :5 [[:1 6]]}
    audience-republic-task.core=> (radius graph)
    11
    audience-republic-task.core=> (diameter graph)
    18
    audience-republic-task.core=> (eccentricity graph :1)
    12

## cli

It is possible to run the app from the console with lein (the way it is proposed for java)

    $ lein run -N 5 -S 10
    {:1 [[:2 4]],
     :2 [[:3 3] [:5 4] [:3 5]],
     :3 [[:4 5]],
     :4 [[:5 7] [:1 6]],
     :5 [[:1 1] [:3 7] [:4 6]]}
    radius is 7
    diameter is 15
    shortest path between :5 and :1 is (:5 :1)
    eccentricity of :4 is 13
