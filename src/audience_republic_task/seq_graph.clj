(ns audience-republic-task.seq-graph)

(defn seq-graph [d g s]
  ((fn rec-seq [explored frontier]
     (lazy-seq
       (if (empty? frontier)
         nil
         (let [[vertex-name weight] (peek frontier)
               neighbors (g vertex-name)]
           (cons [vertex-name weight] (rec-seq
                                        (into explored (map first neighbors))
                                        (into (pop frontier) (remove #(explored (first %)) neighbors))))))))
   #{s} (conj d [s 0])))

(def seq-graph-dfs (partial seq-graph []))
(def seq-graph-bfs (partial seq-graph (clojure.lang.PersistentQueue/EMPTY)))
