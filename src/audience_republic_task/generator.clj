(ns audience-republic-task.generator)

(defn generate-vertices [n]
  (->> (range)
       (take (+ 1 n))
       (drop 1)
       (map str)
       (map keyword)))

(defn rand-weight []
  (+ 1 (rand-int 9)))

(defn vertices->graph [vertices sparseness]
  (let [edges (->> (for [v1 vertices
                         v2 vertices]
                     (when (not= v1 v2) [v1 v2]))
                   (remove nil?)
                   shuffle)
        first-vertex (first vertices)
        initial-graph (loop [vertices vertices
                             graph (sorted-map)]
                        (if (empty? vertices)
                          graph
                          (let [vertex-name (first vertices)
                                mandatory-connection [(or (second vertices) first-vertex) (rand-weight)]
                                rest (rest vertices)]
                            (recur rest
                                   (assoc graph vertex-name [mandatory-connection])))))]
    (loop [sparseness (- sparseness (count vertices))
           graph initial-graph]
      (if (<= sparseness 0)
        graph
        (let [[begin end] (nth edges (- sparseness 1))]
          (recur (dec sparseness)
                 (assoc graph begin (conj (begin graph) [end (rand-weight)]))))))))

(defn generate-graph [n s]
  (let [vertices (generate-vertices n)
        edges (vertices->graph vertices s)]
    edges))
