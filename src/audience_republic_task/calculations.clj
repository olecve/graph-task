(ns audience-republic-task.calculations)

(defn find-vertex-with-lowest-weight [weights explored]
  (->> weights
       (remove #(contains? explored (first %)))
       (sort #(compare (second %1) (second %2)))
       first))

(defn update-weights-&-parents [{:keys [weight weights parents neighbors vertex-name]}]
  (loop [neighbors neighbors
         weights weights
         parents parents]
    (if (empty? neighbors)
      {:parents parents
       :weights weights}
      (let [neighbor (first neighbors)
            neighbor-name (first neighbor)
            neighbor-weight (first (filter #(= (first %) neighbor-name) weights))
            current-weight-or-inf (or neighbor-weight [neighbor-name ##Inf])
            new-weight (+ weight (second neighbor))]
        (recur (rest neighbors)
               (if (> (second current-weight-or-inf) new-weight)
                 (if (some? neighbor-weight)
                   (map #(if (= (first %) neighbor-name) [(first %) new-weight] %) weights)
                   (conj weights [neighbor-name new-weight]))
                 weights)
               (if (> (second current-weight-or-inf) new-weight)
                 (assoc parents neighbor-name vertex-name)
                 parents))))))

(defn find-weights-&-parents [graph begin end]
  (loop [explored #{}
         weights (if (empty? (filter #(= (first %) end) (begin graph)))
                   (conj (begin graph) [end ##Inf])
                   (begin graph))
         parents (into {end nil} (map #(into {(first %) begin}) (begin graph)))]
    (let [vertex (find-vertex-with-lowest-weight weights explored)]
      (if (nil? vertex)
        {:parents parents
         :weights weights}
        (let [weight (second (first (filter #(= (first %) (first vertex)) weights)))
              neighbors ((first vertex) graph)
              weights-&-parents (update-weights-&-parents {:weight      weight
                                                           :weights     weights
                                                           :parents     parents
                                                           :neighbors   neighbors
                                                           :vertex-name (first vertex)})]
          (recur (conj explored (first vertex))
                 (:weights weights-&-parents)
                 (:parents weights-&-parents)))))))

(defn parents->path [parents begin end]
  (try
    (loop [vertex end
           path []]
      (if (= vertex begin)
        (reverse (conj path vertex))
        (recur (vertex parents)
               (conj path vertex))))
    (catch Exception _ [])))

(defn find-shortest-path [graph begin end]
  (parents->path (:parents (find-weights-&-parents graph begin end))
                 begin
                 end))

(defn eccentricity [graph vertex-name]
  (let [begin vertex-name
        ends (remove #{begin} (keys graph))
        weights (map (fn [end]
                       (let [weights-&-parents (find-weights-&-parents graph begin end)
                             result (first (filter #(= end (first %)) (:weights weights-&-parents)))]
                         result))
                     ends)]
    (->> weights (map second) (apply max))))

(defn- eccentricities [graph]
  (->> graph keys (map #(eccentricity graph %))))

(defn radius [graph]
  (apply min (eccentricities graph)))

(defn diameter [graph]
  (apply max (eccentricities graph)))
