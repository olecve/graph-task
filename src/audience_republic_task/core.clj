(ns audience-republic-task.core
  (:require [audience-republic-task.calculations :refer :all]
            [audience-republic-task.generator :refer :all]
            [audience-republic-task.seq-graph :refer :all]
            [clojure.pprint :refer [pprint]])
  (:gen-class))

(defn -main [& args]
  (let [param-to-value (apply hash-map args)
        n (Integer/parseInt (get param-to-value "-N"))
        s (Integer/parseInt (get param-to-value "-S"))
        graph (generate-graph n s)
        vertices (keys graph)
        random-begin (rand-nth vertices)
        random-end (rand-nth (remove #{random-begin} vertices))
        random-eccentricity (rand-nth vertices)]
    (pprint graph)
    (println "radius is" (radius graph))
    (println "diameter is" (diameter graph))
    (println "shortest path between" random-begin "and" random-end "is" (find-shortest-path graph random-begin random-end))
    (println "eccentricity of" random-eccentricity "is" (eccentricity graph random-eccentricity))))
